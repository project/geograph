<?php

/**
 * @file
 * Install, update and uninstall functions for the geograph module.
 */

/**
 * Implements hook_field_schema().
 */
function geograph_field_schema($field) {
  return array(
    'columns' => array(
      'value' => array(
        'type'     => 'int',
        'not null' => FALSE,
      ),
    ),
  );
}

/**
 * Implements hook_requirements().
 */
function geograph_requirements($phase) {

  $requirements = array();

  // Ensure translations don't break during installation.
  $t = get_t();

  switch ($phase) {

    case 'install':

      // Ensure DomDocument is present to parse REST response.
      $requirements['geograph_class_domdocument'] = array('title' => $t('Geograph DomDocument'));
      if (class_exists('DOMDocument')) {
        $requirements['geograph_class_domdocument']['severity'] = REQUIREMENT_OK;
        $requirements['geograph_class_domdocument']['value']    = $t('DomDocument present');
      }
      else {
        $requirements['geograph_class_domdocument']['severity']    = REQUIREMENT_ERROR;
        $requirements['geograph_class_domdocument']['value']       = $t('DomDocument not present');
        $requirements['geograph_class_domdocument']['description'] = $t('Could not find class DomDocument to parse API response. (see @url)', array(
          '@url' => 'http://www.php.net/manual/en/class.domdocument.php',
        ));
      }
      break;

    case 'runtime':

      // Check for api key.
      $requirements['geograph_api_key'] = array('title' => $t('Geograph API key'));
      $api_key = variable_get('geograph_api_key', '');
      if ($api_key) {
        $requirements['geograph_api_key']['severity'] = REQUIREMENT_OK;
        $requirements['geograph_api_key']['value']    = filter_xss($api_key);
      }
      else {
        $requirements['geograph_api_key']['severity']    = REQUIREMENT_ERROR;
        $requirements['geograph_api_key']['value']       = $t('API key is not entered.');
        $requirements['geograph_api_key']['description'] = $t('A unique API-key is required to access the Geograph API, enter key at the <a href="@url_settings">setting page</a>.  If you do not have an API key you need to <a href="@url_geograph">contact Geograph</a>.  Please include the URL so they can take a look.', array(
          '@url_geograph' => 'http://www.geograph.org.uk/contact.php',
          '@url_settings' => '/admin/config/media/geograph',
        ));
      }
  }
  return $requirements;
}

/**
 * Implements hook_schema().
 */
function geograph_schema() {

  // Geograph authors.
  $schema['geograph_author'] = array(
    'description' => 'Authors of Geograph photos.',
    'fields' => array(
      'aid'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Geograph author id.',
      ),
      'name' => array(
        'type'        => 'varchar',
        'not null'    => TRUE,
        'length'      => 64,
        'description' => 'Authors name.',
      ),
      'uid'  => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'description' => 'Drupal user id.',
      ),
    ),
    'foreign keys' => array(
      'user' => array(
        'table'   => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
    'primary key' => array('aid'),
  );

  // Geograph photos.
  $schema['geograph_photo'] = array(
    'description' => 'Joins Drupal files (fid) with Geograph photos (pid).',
    'fields' => array(
      'pid'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Geograph photo id.',
      ),
      'aid'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Geograph author id.',
      ),
      'title' => array(
        'type'        => 'varchar',
        'not null'    => TRUE,
        'length'      => 256,
        'description' => 'Photo title.',
      ),
      'comment' => array(
        'type'        => 'text',
        'not null'    => FALSE,
        'description' => 'Authors comment for photo.',
      ),
      'taken' => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'description' => 'Timestamp of image taken.',
      ),
      'submitted' => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'description' => 'Timestamp of image submission.',
      ),
      'gridref' => array(
        'type'        => 'varchar',
        'not null'    => TRUE,
        'length'      => 8,
        'description' => 'Photo grid reference.',
      ),
      'latitude' => array(
        'type'      => 'numeric',
        'not null'  => FALSE,
        'precision' => 8,
        'scale'     => 5,
        'description' => 'Photo latitude.',
      ),
      'longitude' => array(
        'type'      => 'numeric',
        'not null'  => FALSE,
        'precision' => 8,
        'scale'     => 5,
        'description' => 'Photo longitude.',
      ),
      'height' => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'description' => 'Original photo height (pixels).',
      ),
      'width' => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'description' => 'Original photo width (pixels).',
      ),
      'download' => array(
        'type'        => 'varchar',
        'not null'    => TRUE,
        'length'      => 16,
        'description' => 'Photo download key.',
      ),
    ),
    'foreign keys' => array(
      'geograph_author' => array(
        'table'           => 'geograph_author',
        'columns'         => array('aid' => 'aid'),
      ),
    ),
    'primary key' => array('pid'),
  );

  // Geograph photo sizes.
  $schema['geograph_photo_size'] = array(
    'description' => 'Download sizes of Geograph photos.',
    'fields' => array(
      'fid'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Drupal file id.',
      ),
      'pid'  => array(
        'type'        => 'int',
        'not null'    => TRUE,
        'description' => 'Geograph photo id.',
      ),
      'size'  => array(
        'type'        => 'int',
        'not null'    => FALSE,
        'description' => 'Geograph photo size id.',
      ),
    ),
    'foreign keys' => array(
      'geograph_photo' => array(
        'table'   => 'geograph_photo',
        'columns' => array('pid' => 'pid'),
      ),
      'file_managed' => array(
        'table'   => 'file_managed',
        'columns' => array('fid' => 'fid'),
      ),
    ),
    'primary key' => array('fid', 'pid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function geograph_uninstall() {
  variable_del('geograph_api_key');
  variable_del('geograph_api_size');

  // Should files be deleted also?
  // Files can be used elsewhere and this could be too heavy to do at once.
}
