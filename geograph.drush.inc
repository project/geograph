<?php

/**
 * @file
 * Drush tasks for geograph module.
 */

/**
 * Implements hook_drush_command().
 */
function geograph_drush_command() {
  $items = array();
  $items['geograph_photo_get'] = array(
    'aliases' => array('geograph-pg', 'geo-pg'),
    'description' => 'Downloads photo & meta data for a given Geograph photo id.',
    'arguments' => array(
      'photo_id' => 'Geograph photo id to get, can be a single integer or a comma separated list of integers.',
    ),
  );
  return $items;
}

/**
 * Validates input and invokes geograph_photo_get() in module.
 */
function drush_geograph_photo_get($pids = NULL) {

  // Check photo id argument is present.
  if (is_null($pids)) {
    drush_set_error('photo-id is required.');
  }

  // Validate each photo id.
  $pids = explode(',', $pids);
  $errors = 0;
  foreach ($pids as $pid) {
    if (!is_numeric($pid)) {
      drush_set_error('all photo-ids must be numeric.');
      ++$errors;
    }
  }

  // Download photos if validation passed.
  if (!$errors) {
    geograph_photo_get($pids);
    drush_print('Attempted download of ' . count($pids) . ' Geograph photo(s).');
  }
}
