The [Geograph Britain and Ireland project] (http://www.geograph.org.uk) aims to collect geographically representative photographs and information for every square kilometre of Great Britain and Ireland.

The Geograph module integrates with the details API provided by the Geograph Britain and Ireland project to embed photos into Drupal websites via field and an input filter.

All photos are downloaded to the files/geograph directory (to prevent leaching bandwidth from Geograph) and added to the database via the File API.

Photo licensing
===============

Under the [Creative Commons Licence] (http://creativecommons.org/licenses/by-sa/2.0/), all photos used must be credited to their author. [Find out how to reuse images] (http://www.geograph.org.uk/reuse.php?id=3010866).

Installation
============

* Download & enable the module in the normal manner.
* Obtain an API key from [Geograph] (http://www.geograph.org.uk/contact.php) and enter it on the Geograph [settings page] (/admin/config/media/geograph).

Usage
=====

* Obtain photo id's from Geograph (eg http://www.geograph.org.uk/photo/[photo-id]).
* Add photo id into 'Geograph photo' fields to entities.
* Use the photo id in the 'Geograph photo' input filter on applicable field texts.

Geograph photo field
====================

This module exposes a field named 'Geograph photo' which can be added to entities.  Once added, the widget for the field allows input of a Geograph photo id.

Geograph photo filter
=====================

This module exposes a input filter, 'Geograph photo'. This filter is documented on the filter tips page [link] (/filter/tips).
