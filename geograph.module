<?php

/**
 * @file
 * Defines geograph photo field types.
 */

/**
 * Implements hook_field_info().
 */
function geograph_field_info() {
  return array(
    'geograph' => array(
      'label'             => t('Geograph photo'),
      'description'       => t('This field stores a Geograph photo ID, viewed as a Geograph photo.'),
      'default_widget'    => 'number',
      'default_formatter' => 'geograph_photo_styled',
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function geograph_field_is_empty($item, $field) {
  if (empty($item['value']) && (string) $item['value'] !== '0') {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_load().
 */
function geograph_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {

  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as &$item) {

      // Fetch photo from api.
      geograph_photo_get(array($item['value']));

      // Fetch photo data from database.
      $photo = geograph_photo_load($item['value']);

      // Populate the item.
      foreach ($photo as $key => $value) {
        $item[$key] = $value;
      }
      $item['file'] = file_load($photo['fid']);
    }
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function geograph_field_widget_form_alter(&$element, &$form_state, $context) {

  // Have the number module validate this field as an integer.
  if (array_key_exists('type', $context['field']) and $context['field']['type'] == 'geograph') {
    $element['value']['#number_type'] = 'integer';
    array_push($element['value']['#element_validate'], 'geograph_field_widget_validate');
  }
}

/**
 * Implements hook_field_widget_info_alter().
 */
function geograph_field_widget_info_alter(&$info) {

  // Make the number module allow geograph image ids be added as a number field.
  $info['number']['field types'][] = 'geograph';
}

/**
 * Implements hook_field_formatter_info().
 */
function geograph_field_formatter_info() {

  return array(
    'geograph_photo_styled' => array(
      'label'       => t('Styled image'),
      'field types' => array('geograph'),
      'settings'    => array(
        'geograph_photo_style'         => 'large',
        'geograph_photo_styled_credit' => array('on' => 'on'),
        'geograph_photo_styled_header' => array('on' => 'on'),
        'geograph_photo_styled_footer' => array('on' => 'on'),
      ),
      'description' => t('Scaled image.  Wrapped with configurable credit, title and description attributes.'),
    ),
    'geograph_photo_raw' => array(
      'label'       => t('Raw image'),
      'field types' => array('geograph'),
      'settings'    => array('geograph_photo_style' => 'large'),
      'description' => t('Scaled image only.'),
    ),
    'geograph_src' => array(
      'label'       => t('Url'),
      'field types' => array('geograph'),
      'settings'    => array('geograph_src_link' => 1),
      'description' => t('Url to image.'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function geograph_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'geograph_src':
      $element['geograph_src_link'] = array(
        '#type'          => 'select',
        '#title'         => t('Url display'),
        '#options'       => array(
          1 => t('Link'),
          0 => t('Plain text'),
        ),
        '#default_value' => $settings['geograph_src_link'],
      );
      break;

    case 'geograph_photo_styled':
      $element['geograph_photo_styled_credit'] = array(
        '#type'          => 'checkboxes',
        '#options'       => array('on' => t('Include copyright and licence.')),
        '#default_value' => $settings['geograph_photo_styled_credit'],
      );
      $element['geograph_photo_styled_header'] = array(
        '#type'          => 'checkboxes',
        '#options'       => array('on' => t('Include author title above photo.')),
        '#default_value' => $settings['geograph_photo_styled_header'],
      );
      $element['geograph_photo_styled_footer'] = array(
        '#type'          => 'checkboxes',
        '#options'       => array('on' => t('Include author comment below photo.')),
        '#default_value' => $settings['geograph_photo_styled_footer'],
      );

    case 'geograph_photo_raw':
      $options = array();
      foreach (image_styles() as $name => $style) {
        $options[$name] = $style['label'];
      }
      $element['geograph_photo_style'] = array(
        '#type'          => 'select',
        '#title'         => t('Image style'),
        '#options'       => $options,
        '#default_value' => $settings['geograph_photo_style'],
      );
      break;

  }
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function geograph_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = array();

  switch ($display['type']) {
    case 'geograph_src':
      if ($settings['geograph_src_link']) {
        array_push($summary, 'Linked url');
      }
      else {
        array_push($summary, 'Plain text url');
      }
      break;

    case 'geograph_photo_styled':
      $includes = array(
        'geograph_photo_styled_credit' => t('Include copyright and licence.'),
        'geograph_photo_styled_header' => t('Include author title above photo.'),
        'geograph_photo_styled_footer' => t('Include author comment below photo.'),
      );
      foreach ($includes as $setting => $title) {
        if ($settings[$setting]['on']) {
          array_push($summary, $title);
        }
      }
    case 'geograph_photo_raw':
      $styles = image_styles();
      array_push($summary, t('Image style: @style', array('@style' => $styles[$settings['geograph_photo_style']]['label'])));
      break;
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function geograph_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  $variables = array('settings' => $display['settings']);
  foreach ($items as $delta => $item) {
    $variables['item'] = $item;

    switch ($display['type']) {

      case 'geograph_src':
        $url = file_create_url($variables['item']['file']->uri);
        if ($variables['settings']['geograph_src_link']) {
          $element[$delta] = array('#markup' => l($url, $url));
        }
        else {
          $element[$delta] = array('#markup' => $url);
        }
        break;

      case 'geograph_photo_raw':
      case 'geograph_photo_styled':
      default:
        $info = image_get_info($variables['item']['file']->uri);
        $variables['style_name'] = $variables['settings']['geograph_photo_style'];
        $variables['path']       = $variables['item']['file']->uri;
        $variables['width']      = $info['width'];
        $variables['height']     = $info['height'];
        if ($display['type'] == 'geograph_photo_raw') {
          $element[$delta] = array('#markup' => theme('image_style', $variables));
          break;
        }
        $element[$delta] = array('#markup' => theme('geograph_photo', $variables));
        break;
    }
  }
  return $element;
}

/**
 * Implements hook_filter_info().
 */
function geograph_filter_info() {
  $filters['geograph_photo'] = array(
    'title'            => t('Geograph photo filter'),
    'description'      => t('Embeds Geograph photos in the input text.'),
    'process callback' => 'geograph_filter_geograph_photo_process',
    'tips callback'    => 'geograph_filter_geograph_photo_tips',
  );

  return $filters;
}

/**
 * Implements hook_help().
 */
function geograph_help($path, $arg) {

  switch ($path) {

    // Main module help for the geograph module.
    case 'admin/help#geograph':
      return check_markup(file_get_contents(drupal_get_path('module', 'geograph') . '/README.md'));
  }
}

/**
 * Implements hook_menu().
 */
function geograph_menu() {

  $items['admin/config/media/geograph'] = array(
    'access arguments' => array('administer geograph'),
    'description'      => 'Configure the geograph module.',
    'file'             => 'geograph.admin.inc',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('geograph_admin_settings_form'),
    'title'            => 'Geograph',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function geograph_permission() {
  return array(
    'administer geograph' => array(
      'title'       => t('Administer Geograph'),
      'description' => t('Allows users to view and edit Geograph settings.'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function geograph_theme($existing, $type, $theme, $path) {
  return array(
    'geograph_photo'     => array('render element' => 'item'),
  );
}

/**
 * Implements hook_wysiwyg_plugin().
 */
function geograph_wysiwyg_plugin($editor) {

  $plugins = array();
  switch ($editor) {

    // CKEditor plugin.
    case 'ckeditor':

      $plugins['geograph'] = array(
        'path'       => drupal_get_path('module', 'geograph'),
        'filename'   => 'geograph.ckeditor.js',
        'buttons'    => array('geograph' => t('Geograph photo')),
        'url'        => 'https://drupal.org/project/geograph',
        'internal'   => FALSE,
        'load'       => TRUE,
      );
      break;
  }
  return $plugins;
}

/**
 * Validates a photo can be downloaded for a given photo ID.
 */
function geograph_field_widget_validate($element, &$form_state) {

  // Check Geograph photo ids can be downloaded.
  $value = $element['#value'];
  if (!empty($value)) {
    $errors = geograph_photo_get(array($value));
    if ($errors) {
      form_error($element, filter_xss(theme('item_list', array('items' => $errors))));
    }
  }
}

/**
 * Processes filter filter_geograph_photo.
 */
function geograph_filter_geograph_photo_process($text, $filter) {

  preg_match_all('/\[geograph (.+?)\]/', $text, $matches);
  $doc = new DOMDocument();
  foreach ($matches[1] as $k => $match) {
    $xml = '<geograph ' . html_entity_decode($match) . ' />';
    if (@$doc->loadXML($xml)) {
      $geograph = $doc->getElementsByTagName('geograph')->item(0);
      $pid      = $geograph->getAttribute('pid');
      $style    = $geograph->getAttribute('style');
      $display  = $geograph->getAttribute('display');

      if (!$style) {
        $style = 'large';
      }

      // Check display attribute is present.
      if (!$display) {
        $text = str_replace($matches[0][$k], '<!-- Geograph error: missing display attribute. -->', $text);
        watchdog(
          'geograph',
          "Attribute 'display' missing from input text for geograph photo filter.",
          array(),
          WATCHDOG_ERROR
        );
      }

      if ($pid) {
        geograph_photo_get(array($pid));
        $photo = geograph_photo_load($pid);
        $file = file_load($photo['fid']);
        $variables = array('item' => array('value' => $pid));

        switch ($display) {

          case 'geograph_src':
            $url = file_create_url($file->uri);

            // Capture settings.
            $geograph_src_link = strtolower($geograph->getAttribute('geograph_src_link'));

            // Linked url.
            if ($geograph_src_link != 'false') {
              $text = str_replace($matches[0][$k], l($url, $url), $text);
            }

            // Plain text url.
            else {
              $text = str_replace($matches[0][$k], $url, $text);
            }
            break;

          case 'geograph_photo_styled':

            foreach ($photo as $key => $value) {
              $variables['item'][$key] = $value;
            }

            // Capture settings.
            foreach (array('credit', 'header', 'footer') as $setting) {
              $setting = 'geograph_photo_styled_' . $setting;
              $value = strtolower($geograph->getAttribute($setting));
              if ($value == 'false') {
                $variables['settings'][$setting]['on'] = 0;
              }
              else {
                $variables['settings'][$setting]['on'] = 'on';
              }
            }

          case 'geograph_photo_raw':
            $info = image_get_info($file->uri);
            $variables['width']  = $info['width'];
            $variables['height'] = $info['height'];
            $variables['style_name'] = $style;
            $variables['path']       = $file->uri;
            if ($display == 'geograph_photo_raw') {
              $text = str_replace($matches[0][$k], theme('image_style', $variables), $text);
              break;
            }
            $text = str_replace($matches[0][$k], theme('geograph_photo', $variables), $text);
            break;

          // Invalid display attribute.
          default:
            $text = str_replace($matches[0][$k], '<!-- Geograph error: invalid display attribute. -->', $text);
            watchdog(
              'geograph',
              "Attribute 'display' invalid from input text for geograph photo filter.",
              array(),
              WATCHDOG_ERROR
            );
            break;
        }
      }

      // Missing pid attribute.
      else {
        $text = str_replace($matches[0][$k], '<!-- Geograph error: missing pid attribute. -->', $text);
        watchdog(
          'geograph',
          "Attribute 'pid' missing from input text for geograph photo filter.",
          array(),
          WATCHDOG_ERROR
        );
      }
    }

    // Cannot load xml.
    else {
      $text = str_replace($matches[0][$k], '<!-- Geograph error: invalid placeholder. -->', $text);
      watchdog(
        'geograph',
        "Placeholder markup could not be parsed (@xml).",
        array('@xml' => $xml),
        WATCHDOG_ERROR
      );
    }
  }
  return $text;
}

/**
 * Provides tips for filter filter_geograph_photo.
 */
function geograph_filter_geograph_photo_tips($filter, $format, $long = FALSE) {
  if ($long) {

    $demo_pid = 3010866;
    $demo_styled = '[geograph pid="' . $demo_pid . '" display="geograph_photo_styled" style="large"]';
    $demo_raw    = '[geograph pid="' . $demo_pid . '" display="geograph_photo_raw" style="thumbnail"]';
    $demo_src    = '[geograph pid="' . $demo_pid . '" display="geograph_src"]';
    $t = array(
      '@url_image_style'      => '/admin/config/media/image-styles',
      '@url_ccl'              => 'http://creativecommons.org/licenses/by-sa/2.0/',
      '@url_reuse'            => 'http://www.geograph.org.uk/reuse.php?id=' . $demo_pid,
      '!code_disable_title'   => '<code>geograph_photo_styled_header="false"</code>',
      '!code_disable_comment' => '<code>geograph_photo_styled_footer="false"</code>',
      '!code_disable_credit'  => '<code>geograph_photo_styled_credit="false"</code>',
      '!code_disable_link'    => '<code>geograph_src_link="false"</code>',
      '!code_display_styled'  => '<code>display="geograph_photo_styled"</code>',
      '!code_display_raw'     => '<code>display="geograph_photo_raw"</code>',
      '!code_display_src'     => '<code>display="geograph_src"</code>',
      '!code_demo_styled'     => "<code>$demo_styled</code>",
      '!code_demo_raw'        => "<code>$demo_raw</code>",
      '!code_demo_src'        => "<code>$demo_src</code>",
    );

    $output = array(
      'intro' => array(
        '#prefix' => '<p>',
        '#markup' => t('Embed <a href="@url_help">Geograph</a> photos.', array(
          '@url_help' => '/admin/help/geograph',
        )),
        '#suffix' => '</p>',
      ),
      'copyright' => array(
        '#prefix' => '<p><em>',
        '#markup' => t('Under the <a href="@url_ccl">Creative Commons Licence</a>, all photos used must be credited to their author. <a href="@url_reuse">Find out how to reuse images</a>.'),
        '#suffix' => '</em></p>',
      ),
      'geograph_photo_styled' => array(
        'header' => array(
          '#prefix' => '<h4>',
          '#markup' => t('Styled image'),
          '#suffix' => '</h4>',
        ),
        'desc' => array(
          '#prefix' => '<p><div style="float:right;padding-left:5px;">' . geograph_filter_geograph_photo_process($demo_styled, NULL) . '</div>',
          '#markup' => t('To use this display, add the following key="value" pair to the input format text:<br />!code_display_styled<br />The result will be an image, styled with a predefined Drupal <a href="@url_image_style">image style</a> (default styles are large, medium &amp; thumbnail). The image will be wrapped with its title, comment and copyright & licencing information supplied from the Geograph API.  The photo shown (right) was created with the following example filter text:<br >!code_demo_styled<br />The title, comment and copyright and licence text can be disabled by adding the following key="value" pairs to the input filter text:', $t),
          '#suffix' => '</p>',
        ),
        'options' => array(
          '#theme' => 'item_list',
          '#items' => array(
            t('Disable credit: !code_disable_credit.<br /><em>You must make your own arrangements for complying with the <a href="@url_ccl">Creative Commons Licence</a> with this option.</em>', $t),
            t('Disable title: !code_disable_title.', $t),
            t('Disable comment: !code_disable_comment.', $t),
          ),
        ),
      ),
      'geograph_photo_raw' => array(
        'header' => array(
          '#prefix' => '<h4>',
          '#markup' => t('Raw image'),
          '#suffix' => '</h4>',
        ),
        'desc' => array(
          '#prefix' => '<p><div style="float:left;padding-right:5px;">' . geograph_filter_geograph_photo_process($demo_raw, NULL) . '</div>',
          '#markup' => t('To use this display, add the following key="value" pair to the input format text:<br />!code_display_raw<br />An image only (img tags), styled with a predefined Drupal <a href="@url_image_style">image style</a> (default styles are large, medium &amp; thumbnail).  The image shown (left) was created with the following example filter text:<br >!code_demo_raw<br /><em>You must make your own arrangements for complying with the <a href="@url_ccl">Creative Commons Licence</a> with this display.</em>', $t),
          '#suffix' => '</p>',
        ),
      ),
      'geograph_src' => array(
        'header' => array(
          '#prefix' => '<h4>',
          '#markup' => t('Url'),
          '#suffix' => '</h4>',
        ),
        'desc' => array(
          '#prefix' => '<p>' . geograph_filter_geograph_photo_process($demo_src, NULL) . '<br />',
          '#markup' => t('To use this display, add the following key="value" pair to the input format text:<br />!code_display_src<br />A url to the unstyled image. The link above was created with the following example filter text:<br >!code_demo_src<br /><em>You must make your own arrangements for complying with the <a href="@url_ccl">Creative Commons Licence</a> with this display.</em><br />The linkifcation can be disabled with the following key="value" pair to the input filter text:', $t),
          '#suffix' => '</p>',
        ),
        'options' => array(
          '#theme' => 'item_list',
          '#items' => array(
            t('Disable linkification: !code_disable_link.', $t),
          ),
        ),
      ),
    );
    return drupal_render($output);
  }
  else {
    return t('Embed <a href="@url_help">Geograph</a> photos as described in <a href="@url_tips">compose tips</a>.', array(
      '@url_help' => '/admin/help/geograph',
      '@url_tips' => '/filter/tips',
    ));
  }
}

/**
 * Downloads Geograph photos to files directory & created file db entry.
 */
function geograph_photo_get($pids, $refresh = FALSE) {

  $size = variable_get('geograph_api_size', 800);
  $url = array(
    'http://api.geograph.org.uk',
    'api',
    'photo',
    0,
    variable_get('geograph_api_key', ''),
  );

  $errors = array();
  foreach ($pids as $pid) {

    // Check photo is not already in the database.
    $photo = geograph_photo_load($pid);
    if ($photo and !$refresh) {
      continue;
    }

    // Construct api url for photo.
    $url[3] = $pid;
    $result = drupal_http_request(implode('/', $url));
    if ($result->code == 200) {
      $xml = $result->data;
    }
    else {
      watchdog(
        'geograph',
        'Cannot request from api (code @code).',
        array('@code' => $result->code),
        WATCHDOG_ERROR,
        l(t('api'), implode('/', $url))
      );
      array_push($errors, t('Cannot request from api (code @code) @url.', array(
        '@code' => $result->code,
        '@url'  => implode('/', $url),
      )));
      continue;
    }

    $doc = new DOMDocument();
    if ($doc->loadXML($xml)) {

      // Request image.
      $path = $pid;
      if (strlen($path) % 2 != 0) {
        $path = '0' . $path;
      }
      $path = str_split($path, 2);
      $filename = array(array_pop($path), NULL, 'jpg');
      array_unshift($path, 'geograph');
      $uri = 'public://' . implode('/', $path);

      file_prepare_directory($uri, FILE_CREATE_DIRECTORY);
      $src = $doc->getElementsByTagName('img')->item(0)->getAttribute('src');
      preg_match('/_(.+?)\./', $src, $matches);
      $key = $matches[1];
      $query = array(
        'id'       => $pid,
        'download' => $key,
      );
      $requested = FALSE;

      // Attempt photo downloads, on failure reduce size and retry.
      foreach (array('original', 1024, 800, 640) as $dlsize) {

        // Deduce size parameter, dont include for 640 photos.
        if ($dlsize != 640) {
          $query['size'] = $dlsize;
        }
        else {
          unset($query['size']);
        }

        // Skip sizes larger than our request.
        if (!$requested and $size != $dlsize) {
          continue;
        }

        // Request sizes equal or smaller than our request.
        else {
          $requested = $dlsize;
          $download = url('http://www.geograph.org.uk/reuse.php', array('query' => $query));
          $result = drupal_http_request($download);

          // Handle photo download error.
          if ($result->code != 200) {
            watchdog(
              'geograph',
              'Cannot download photo (code @code).',
              array('@code' => $result->code),
              WATCHDOG_ERROR,
              l(t('url'), $download)
            );
            array_push($errors, t('Cannot download photo (code @code) @url.', array(
              '@code' => $result->code,
              '@url'  => $download,
            )));
            break;
          }
        }

        // Stop attempting download once after first success.
        if ($result->code == 200 and strpos($result->headers['content-disposition'], 'geograph-error.jpg') === FALSE) {
          break;
        }
      }

      // Warn if requested size was not available.
      if ($size != $requested) {
        drupal_set_message(
          t(
            'Source size @size was not available for download, falling back to @dlsize.',
            array(
              '@size' => $size,
              '@dlsize' => $requested,
            )
          ),
          'warning'
        );
        watchdog(
          'geograph',
          'Source size @size was not available for download, falling back to @dlsize.',
          array(
            '@size' => $size,
            '@dlsize' => $requested,
          ),
          WATCHDOG_NOTICE
        );
      }

      if ($result->code == 200) {

        watchdog(
          'geograph',
          'Photo downloaded (id: @pid).',
          array('@pid' => $pid),
          WATCHDOG_INFO,
          l(t('url'), $download)
        );

        $filename[1] = $requested;
        $file = file_save_data($result->data, $uri . '/' . implode('.', $filename), FILE_EXISTS_REPLACE);

        // Insert/update photo size table.
        db_merge('geograph_photo_size')->key(array('pid' => $pid, 'fid' => $file->fid))->fields(array(
          'size' => is_numeric($requested) ? $requested : NULL,
        ))->execute();

        // Insert/update authors table for photo.
        $user = $doc->getElementsByTagName('user')->item(0);
        preg_match('/(\d+)$/', $user->getAttribute('profile'), $matches);
        $aid = $matches[0];
        db_merge('geograph_author')->key(array('aid' => $aid))->fields(array(
          'name' => check_plain($user->nodeValue),
        ))->execute();

        // Insert/update photo table for photo.
        $original = $doc->getElementsByTagName('original')->item(0);
        if ($original) {
          $height = $original->getAttribute('height');
          $width  = $original->getAttribute('width');
        }
        else {
          $height = $width = NULL;
        }
        db_merge('geograph_photo')->key(array('pid' => $pid))->fields(array(
          'title'     => filter_xss(html_entity_decode($doc->getElementsByTagName('title')->item(0)->nodeValue, ENT_QUOTES, 'ISO-8859-1')),
          'comment'   => filter_xss(html_entity_decode($doc->getElementsByTagName('comment')->item(0)->nodeValue, ENT_QUOTES, 'ISO-8859-1')),
          'aid'       => $aid,
          'taken'     => strtotime($doc->getElementsByTagName('taken')->item(0)->nodeValue),
          'submitted' => strtotime($doc->getElementsByTagName('submitted')->item(0)->nodeValue),
          'gridref'   => filter_xss(html_entity_decode($doc->getElementsByTagName('gridref')->item(0)->nodeValue, ENT_QUOTES, 'ISO-8859-1')),
          'latitude'  => NULL,
          'longitude' => NULL,
          'height'    => $height,
          'width'     => $width,
          'download'  => filter_xss($key),
        ))->execute();
      }
    }
  }
  return $errors;
}

/**
 * Fetches Geograph photos details from db.
 */
function geograph_photo_load($pid) {

  // Fetch largest available photo data from database.
  $query = db_select('geograph_photo', 'GP');
  $query->join('geograph_author', 'GA', 'GA.aid = GP.aid');
  $query->join('geograph_photo_size', 'GPS', 'GPS.pid = GP.pid');
  $query->fields('GP', array(
    'title',
    'comment',
    'taken',
    'submitted',
    'gridref',
  ));
  $query->fields('GA', array('aid', 'name', 'uid'));
  $query->fields('GPS', array('size', 'fid'));
  $query->addExpression('CASE WHEN size IS NULL THEN 9999 ELSE size END', 'order_by');
  $query->condition('GP.pid', $pid);
  $query->orderBy('order_by', 'DESC');
  $query->range(0, 1);
  $result = $query->execute()->fetchAssoc();
  return $result;
}

/**
 * Returns HTML for a Geograph photo.
 */
function theme_geograph_photo(&$variables) {

  // Fetch styled image data.
  $dimensions = array('width' => $variables['width'], 'height' => $variables['height']);
  image_style_transform_dimensions($variables['style_name'], $dimensions);

  $output = array();
  $output['comment'] = array('#markup' => '<!-- Geograph photo #' . $variables['item']['value'] . '  -->');
  $output['wrapper'] = array(
    '#prefix' => '<div class="geograph-wrapper" style="width:' . $dimensions['width'] . 'px;">',
    '#suffix' => '</div>',
  );

  // Add title as the image header if it is present.
  if ($variables['settings']['geograph_photo_styled_header']['on'] and $variables['item']['title']) {
    $output['wrapper']['child']['header'] = array(
      '#prefix' => '<div class="photo-header">',
      '#markup' => $variables['item']['title'],
      '#suffix' => '</div>',
    );
  }

  // Apply image markup.
  $output['wrapper']['child']['photo-wrapper'] = array(
    '#prefix' => '<div class="photo-wrapper" style="width:' . $dimensions['width'] . 'px;height:' . $dimensions['height'] . 'px;">',
    '#suffix' => '</div>',
    'child'   => array(
      'photo' => array('#markup' => theme('image_style', $variables)),
    ),
  );

  // Overlay credit & licence information.
  if ($variables['settings']['geograph_photo_styled_credit']['on']) {
    $output['wrapper']['child']['photo-wrapper']['child']['credit'] = array(
      '#prefix' => '<div class="photo-credit">',
      '#markup' => t(
        '&copy; Copyright <a href="@url-profile" title="View Geograph profile for @author-name">@author-name</a> and licensed for reuse under the <a href="@url-licence">Creative Commons Licence</a>.',
        array(
          '@author-name' => $variables['item']['name'],
          '@url-profile' => 'http://www.geograph.org.uk/profile/' . $variables['item']['aid'],
          '@url-licence' => 'http://creativecommons.org/licenses/by-sa/2.0',
        )
      ),
      '#suffix' => '</div>',
    );
  }

  // Apply comment as image footer if it is present.
  if ($variables['settings']['geograph_photo_styled_footer']['on'] and $variables['item']['comment']) {
    $output['wrapper']['child']['footer'] = array(
      '#prefix' => '<div class="photo-footer">',
      '#markup' => $variables['item']['comment'],
      '#suffix' => '</div>',
    );
  }

  return drupal_render($output);
}
