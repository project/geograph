(function($) {

  CKEDITOR.dialog.add('geographDialog', function(editor){
    return {
      title : 'Geograph photo selection',
      minWidth : 400,
      minHeight : 200,
      contents :
      [
        {
          id : 'general',
          label : 'Settings',
          elements :
          [
            {
              type : 'html',
              html : 'Enter the details of the required geograph photo and theming.'
            },
            {
              type : 'text',
              id : 'pid',
              label : 'Photo id',
              validate : CKEDITOR.dialog.validate.regex(/^(\d+)$/, 'The photo id must be numeric.'),
              required : true,
              commit : function(data) {
                data.pid = this.getValue();
              }
            },
            {
              type : 'select',
              id : 'display',
              label : 'Display',
              items :
              [
                [ 'Styled image', 'geograph_photo_styled' ],
                [ 'Raw image'   , 'geograph_photo_raw'    ],
                [ 'Url'         , 'geograph_src'          ],
              ],
              commit : function(data) {
                data.display = this.getValue();
              }
            },
          ]
        }
      ],
      onOk : function(){
        var dialog = this, data = {};
        this.commitContent(data);
        editor.insertText('[geograph pid="' + data.pid + '" display="' + data.display + '"]');
      }
    };
  });

  CKEDITOR.plugins.add('geograph', {
    init: function (editor) {

      editor.addCommand('geographDialog', new CKEDITOR.dialogCommand('geographDialog'));
      editor.ui.addButton('geograph', {
        label: 'Insert a geograph photo',
        command: 'geographDialog',
        icon: this.path + 'geograph.ico'
      });
    }
  });

})(jQuery);
