<?php

/**
 * @file
 * Administrative page callbacks for the Geograph module.
 */

/**
 * Menu callback for settings form.
 */
function geograph_admin_settings_form($form, $form_state) {

  $form['geograph_api_key'] = array(
    '#type'             => 'textfield',
    '#title'            => t('API key'),
    '#default_value'    => variable_get('geograph_api_key', ''),
    '#description'      => t('A unique API-key is required to access the Geograph API.  If you do not have an API key you need to <a href="@url">contact Geograph</a>.  Please include the URL so they can take a look.', array('@url' => 'http://www.geograph.org.uk/contact.php')),
  );
  $form['geograph_api_size'] = array(
    '#type'             => 'select',
    '#title'            => t('Source dimensions'),
    '#options'          => array(
      640        => t('640x480'),
      800        => t('800x600'),
      1024       => t('1024x768'),
      'original' => t('Original'),
    ),
    '#default_value'    => variable_get('geograph_api_size', 800),
    '#description'      => t('Dimensions for source photo downloads, larger sizes will consume more disk space but offer higher quality.'),
  );
  return system_settings_form($form);
}
